
import { Route } from '@angular/router';
import { LibrosShowComponent } from 'src/app/libros-show/libros-show.component';
import { LibrosAddComponent } from 'src/app/libros-add/libros-add.component';


export const LIBROS_ROUTES:Route[] = [
    {path: 'libros', component: LibrosShowComponent},
    {path: 'libros/add', component: LibrosAddComponent},
    
  ];