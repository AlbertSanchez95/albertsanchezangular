import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibrosShowComponent } from '../libros-show/libros-show.component';
import { LibroDescComponent } from '../libro-desc/libro-desc.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LIBROS_ROUTES } from './routes/libros-routes';
import { LibrosAddComponent } from '../libros-add/libros-add.component';


@NgModule({
  declarations: [
    LibrosShowComponent,
    LibroDescComponent,
    LibrosAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(LIBROS_ROUTES)
  ],
  exports:[
    LibrosShowComponent
  ]
})
export class LibrosModule { }
