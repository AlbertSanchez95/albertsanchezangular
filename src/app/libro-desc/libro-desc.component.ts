
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Libro } from 'interfaces/libro';
import { BibliotecaService } from '../biblioteca.service';
@Component({
  selector: 'albert-libro-desc',
  templateUrl: './libro-desc.component.html',
  styleUrls: ['./libro-desc.component.css']
})
export class LibroDescComponent implements OnInit {
  @Input() libro: Libro;
  @Output() deleteOneLibro=new EventEmitter<Libro>();
  constructor() { }

  
    
  ngOnInit() {
  }
 
}
