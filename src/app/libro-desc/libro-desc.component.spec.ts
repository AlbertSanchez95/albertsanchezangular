import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibroDescComponent } from './libro-desc.component';

describe('LibroDescComponent', () => {
  let component: LibroDescComponent;
  let fixture: ComponentFixture<LibroDescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibroDescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibroDescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
