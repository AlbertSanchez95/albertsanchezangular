
import { Route } from "@angular/router";

export const APP_ROUTES: Route[]=[
  
    {path: 'libros', loadChildren:'./libros/libros.module#LibrosModule'},

  ];