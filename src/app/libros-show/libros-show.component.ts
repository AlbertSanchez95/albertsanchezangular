import { Component, OnInit } from '@angular/core';
import { Libro } from './../../../interfaces/libro';
import { BibliotecaService } from './../biblioteca.service';

@Component({
  selector: 'albert-libros-show',
  templateUrl: './libros-show.component.html',
  styleUrls: ['./libros-show.component.css']
})
export class LibrosShowComponent implements OnInit {
  filterSearch : string='';

  libros: Libro[]=[];
  
  constructor(private LibrosService: BibliotecaService) {}

  ngOnInit() {
    this.LibrosService.getLibros().subscribe(
      libro=>this.libros=libro,
      error=>console.error(error),
      ()=>console.log("Libros cargados")
    );
  }
  filtrarPrecio()
  {
    this.libros.sort(function(a: Libro, b:Libro){
      if(a.precio>b.precio)
        return 1;
      else if(a.precio==b.precio)
        return 0;
      else
        return -1;
      });
  }
  deleteLibro(libro:Libro)
  {
    this.LibrosService.deleteLibros(libro.cod).subscribe(
      libros=>this.libros.splice(libro.cod, 1),
      error=>console.error(error),
      ()=>console.log("Eventos borrados")
    );
    //this.eventos=[...this.eventos];
    this.LibrosService.getLibros().subscribe(
      libros=>this.libros=libros,
      error=>console.error(error),
      ()=>console.log("Eventos cargados")
    );
    
  }
 
}
