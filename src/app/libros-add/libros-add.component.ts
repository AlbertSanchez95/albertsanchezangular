import { Component, OnInit } from '@angular/core';
import { BibliotecaService } from '../biblioteca.service';
import { Router } from '@angular/router';
import { Libro } from 'interfaces/libro';
import { NewLibro, deleteLibro } from './../biblioteca.service';

@Component({
  selector: 'albert-libros-add',
  templateUrl: './libros-add.component.html',
  styleUrls: ['./libros-add.component.css']
})
export class LibrosAddComponent implements OnInit {

  constructor(private LibroService: BibliotecaService, private router: Router) { }

  libros: Libro[]=[];
  ngOnInit() {
  }

  newLibro: Libro ={
    cod: 0,
    isbn: 0,
    precio: 0,
    titulo:"",
    imagen:"",
    url:"",
    cod_ac:0
  };

  addLibro(){
    this.LibroService.addLibros(this.newLibro).subscribe(
      libro=>this.libros.push(libro),
      error=>console.error(error),
      ()=>console.log("Eventos cargados")
    );
    this.newLibro={
      cod: 0,
      isbn: 0,
      precio: 0,
      titulo:"",
      imagen:"",
      url:"",
      cod_ac:0
    };
    
    //location.reload();
    this.router.navigate(['libros']);
  }

  changeImage(fileInput:HTMLInputElement){
    if(!fileInput||fileInput.files.length == 0)
      return;
    const reader: FileReader = new FileReader();

    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e =>{
      this.newLibro.imagen=reader.result.toString();
    })
  }
}

