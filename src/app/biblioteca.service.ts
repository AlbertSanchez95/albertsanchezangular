import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Libro } from 'interfaces/libro';
import { Observable, throwError } from 'rxjs';
import {map, catchError, retry} from 'rxjs/operators';

export interface LibroResponse{
  ok:boolean;
  libro?:Libro[];
  error?: string;
}
export interface deleteLibro{
  ok:boolean;
  error?:string;
}
export interface NewLibro{
  ok: boolean;
  libro: Libro;
  error?: string;
}
@Injectable({
  providedIn: 'root'
})

export class BibliotecaService {
  private libroUrl='http://localhost:8080';

  constructor(private http: HttpClient) { }
  getLibros(){
    return this.http.get<Libro[]>(this.libroUrl+ "/libros").pipe(
      map(
        response => response
      ),catchError((resp:HttpErrorResponse) => throwError("Error cargando los libros"))
    );
  }
  getLibro(idLibro:number){
    return this.http.get<Libro[]>(this.libroUrl+ "/libros/"+idLibro).pipe(
      map(
        response => response
      ),catchError((resp:HttpErrorResponse) => throwError("Error cargando los libros"))
    );
  }

  addLibros(libro: Libro):Observable<Libro>{
    return this.http.post<NewLibro>(this.libroUrl+"/libros/add", libro).pipe(catchError((resp: HttpErrorResponse)=>throwError('Error insertando un libro. Código de servidor: ${resp.status}. Mensaje: ${res.message}')),
    map(resp=>{
      if(!resp.ok){throw resp.error}
      return resp.libro;
    }
    ));
  }
  deleteLibros(idLibro: number): Observable<boolean>{
    return this.http.delete<deleteLibro>(this.libroUrl + '/libros/' + idLibro).pipe(catchError((resp:HttpErrorResponse)=>throwError('Error insertando un Libro. Código de servidor: ${resp.status}. Mensaje: ${res.message}')),
    map(resp=>{
      if(!resp.ok){throw resp.error}
      return resp.ok;
    }
    ));
  }
}
