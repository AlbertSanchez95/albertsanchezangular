import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LibrosShowComponent } from './libros-show/libros-show.component';
import { LibroDescComponent } from './libro-desc/libro-desc.component';
import { LibrosModule } from './libros/libros.module';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { LibrosAddComponent } from './libros-add/libros-add.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
  
  ],
  imports: [
    BrowserModule,
    LibrosModule,
    RouterModule,
    RouterModule.forRoot(APP_ROUTES),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
