export interface Libro {
    cod?: number;
    isbn: number;
    precio: number;
    titulo: string;
    imagen: string;
    url: string;
    cod_ac?: number;
}
